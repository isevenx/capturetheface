#include "options.h"
#include <QDebug>
#include "ui_options.h"


Options::Options(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Options)
{
    ui->setupUi(this);
}

Options::~Options()
{
    delete ui;
}

void Options::on_buttonBox_accepted()
{
    int bnw=0;
    if (ui->checkBox->isChecked()) bnw=1;
    if (ui->checkBox_2->isChecked()) bnw=2;
    if (ui->checkBox->isChecked() && ui->checkBox_2->isChecked()) bnw=3;
    emit sendOptions(ui->lineEdit->text().toInt(),ui->lineEdit_2->text().toInt(),bnw);

}
