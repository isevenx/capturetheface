#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#define FEAT_FACE_FILE "/home/rihard/opencv/data/haarcascades/haarcascade_frontalface_alt.xml"
#define FEAT_EYE_FILE "/home/rihard/opencv/data/haarcascades/haarcascade_mcs_eyepair_big.xml"
#define FEAT_NOSE_FILE "/home/rihard/opencv/data/haarcascades/haarcascade_mcs_nose.xml"
#define FEAT_MOUTH_FILE "/home/rihard/opencv/data/haarcascades/haarcascade_mcs_mouth.xml"

#include "options.h"

#include <QMainWindow>
#include <QFileDialog>
#include <QVector>
#include <QDebug>

#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <QCoreApplication>



namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void findFiles(QDir path, QString indent);

    QString dirName,spath;
    QStringList fileName;
    QDir currentDir;
    QFileInfoList list,files;
    QList<QFileInfo> info;

    int hasjpg,faceCount,chPic,Width,Height,BnW;

    Options optionW;


    ~MainWindow();


public slots:
    void chooseFolder1();
    void chooseFolder2();
    void makePictures();
    void findPictures();
    void findDir();
    void faceDetect(int picNum);
    void oShow();
    void changeOptions(int width,int height,int bnw);


private:

    Ui::MainWindow *ui;
};


#endif // MAINWINDOW_H
