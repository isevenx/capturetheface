#-------------------------------------------------
#
# Project created by QtCreator 2014-06-18T15:00:59
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CaptureTheFace
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    options.cpp

HEADERS  += mainwindow.h \
    options.h

FORMS    += mainwindow.ui \
    options.ui


INCLUDEPATH += /home/usr/opencv
INCLUDEPATH += home/usr/opencv/data/haarcascades/
LIBS += -L/home/usr/opencv/release
LIBS += -lopencv_calib3d -lopencv_contrib -lopencv_features2d \
        -lopencv_flann -lopencv_imgproc -lopencv_ml \
        -lopencv_objdetect -lopencv_video -lopencv_highgui -lopencv_core


