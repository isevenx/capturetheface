#ifndef OPTIONS_H
#define OPTIONS_H

#include <QDialog>
#include <mainwindow.h>

namespace Ui {
class Options;
}

class Options : public QDialog
{
    Q_OBJECT

public:
    explicit Options(QWidget *parent = 0);
    ~Options();

private slots:
    void on_buttonBox_accepted();
signals:
    void sendOptions(int width,int height,int bnw);

private:
    Ui::Options *ui;
};

#endif // OPTIONS_H
