#include "mainwindow.h"
#include "ui_mainwindow.h"

using namespace std;
using namespace cv;


void MainWindow::oShow(){     // show options window
   optionW.show();
}
void MainWindow::changeOptions(int width,int height,int bnw){
    Width=width;
    Height=height;
    BnW=bnw;
}

void MainWindow::findDir(){     // show file Dir
    for (int i=0;i<info.size();i++){
        if(ui->listWidget->currentItem()->text().endsWith(info[i].fileName())){
            ui->lineEdit->clear();
            ui->lineEdit->insert(info[i].filePath());
        }
    }
    chPic=ui->listWidget->currentRow();
}
void MainWindow::chooseFolder2(){     // browse -2-
    dirName = QFileDialog::getExistingDirectory(this,
    tr("Result Directory"),"/home",QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    if(!dirName.isEmpty()){
            ui->comboBox_2->addItem(dirName);
            int index = ui->comboBox_2->findText(dirName);
            ui->comboBox_2->setCurrentIndex(index);
    }
}
void MainWindow::chooseFolder1(){     // browse -1-
    dirName = QFileDialog::getExistingDirectory(this,
    tr("Search Directory"),"/home",QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    if(!dirName.isEmpty()){
            ui->comboBox->addItem(dirName);
            int index = ui->comboBox->findText(dirName);
            ui->comboBox->setCurrentIndex(index);
    }
}


void MainWindow::faceDetect(int picNum)                   // -4-
{

    cv::Mat mOrigImage = cv::imread(info[picNum].filePath().toStdString());
    QString qfileName;
    cv::Mat mElabImage;
    mOrigImage.copyTo( mElabImage );
    cv::CascadeClassifier mFaceDetector;
    cv::CascadeClassifier mEyeDetector;
    cv::CascadeClassifier mMouthDetector;
    cv::CascadeClassifier mNoseDetector;

    if( mFaceDetector.empty() )
        mFaceDetector.load( FEAT_FACE_FILE );
    if( mEyeDetector.empty() )
        mEyeDetector.load( FEAT_EYE_FILE );
    if( mNoseDetector.empty() )
        mNoseDetector.load( FEAT_NOSE_FILE );
    if( mMouthDetector.empty() )
        mMouthDetector.load( FEAT_MOUTH_FILE );

    vector<cv::Rect> faceVec;

    float scaleFactor = 1.1f; // Change Scale Factor to change speed
    mFaceDetector.detectMultiScale(mOrigImage,faceVec,scaleFactor);

    for( size_t i=0; i<faceVec.size(); i++ )
    {
        cv::rectangle( mElabImage, faceVec[i], CV_RGB(255,0,0), 2 );
        cv::Mat face = mOrigImage( faceVec[i] );
        faceCount++;
        cv::Size size(Width,Height);
        cv::Mat rFace;

        //cv::namedWindow("Face");
        //cv::imshow( "Face", face );
        qfileName=ui->comboBox_2->currentText()+"/"+info[picNum].fileName();
        qfileName.chop(4);
        if ((BnW==2) || (BnW==3)) qfileName=ui->comboBox_2->currentText()+"/"+QString::number(faceCount)+".jpg";
        else qfileName+="_"+QString::number(i+1)+".jpg";
        if((BnW==1) || (BnW==3)) cvtColor(face,face,CV_BGR2GRAY);
        cv::resize(face,rFace,size);
        cv::imwrite(qfileName.toStdString(),rFace);
    }
        //cv::namedWindow("My Image",WINDOW_NORMAL);
        //cv::imshow("My Image", mElabImage);

}

void MainWindow::makePictures()                           // -3-
{
    QString path=ui->comboBox_2->currentText()+"/";
    QDir dir(path);
    if(!dir.exists()) dir.mkpath(path);
    if(!info.isEmpty()){
        for(chPic=0;chPic<info.size();chPic++)
            faceDetect(chPic);
    }    
}
void MainWindow::findFiles(QDir path, QString indent)     // -2-
{
    currentDir = QDir(path);
    indent += "           ";
    list = currentDir.entryInfoList(QDir::Files | QDir::Dirs | QDir::NoDotAndDotDot);
    foreach(QFileInfo finfo, list) {
       if(finfo.fileName().endsWith(".jpg")|finfo.fileName().endsWith(".png")) {
           hasjpg++;
           ui->listWidget->addItem(indent+finfo.fileName());
           info << finfo;
       }
       if (finfo.isDir()&&ui->checkBox->isChecked()) {
           findFiles(QDir(finfo.absoluteFilePath()), indent);
       }
    }
}
void MainWindow::findPictures()                           // -1-
{
    ui->label_2->setText(" ");
    hasjpg=0;
    info.clear();
    ui->listWidget->clear();
    spath = ui->comboBox->currentText();
    findFiles(spath,"");
    if (ui->checkBox_2->isChecked()) {
        makePictures();
        ui->label_2->setText("Result:  "+QString::number(hasjpg)+" processed, "+QString::number(faceCount)+" faces made");
    }
    else ui->label_2->setText("Result:  "+QString::number(hasjpg)+" found");

}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);  
    chPic=0;
    Width=70;
    Height=70;
    BnW=0;
    connect(ui->pushButton,SIGNAL(clicked()),SLOT(findPictures()));
    connect(ui->pushButton_2,SIGNAL(clicked()),SLOT(chooseFolder1()));
    connect(ui->pushButton_3,SIGNAL(clicked()),SLOT(chooseFolder2()));
    connect(ui->pushButton_4,SIGNAL(clicked()),SLOT(oShow()));
    connect(ui->listWidget,SIGNAL(itemClicked(QListWidgetItem*)),SLOT(findDir()));
    connect(&optionW,SIGNAL(sendOptions(int,int,int)),SLOT(changeOptions(int,int,int)));
    ui->comboBox->addItem("/home/rihard/Pictures");
    ui->comboBox_2->addItem("/home/rihard/Result");

}
MainWindow::~MainWindow()
{
    delete ui;
}
